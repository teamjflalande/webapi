using System;

namespace WebApi.Entities
{
    public class PasswordReset
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string HashToken { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool IsReset { get; set; }
        public bool IsEnable { get; set; }     
    }
}
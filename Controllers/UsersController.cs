﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Entities;
using WebApi.Models.Users;
using System.Threading.Tasks;
using System.Web;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UsersController(
            IUserService userService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody]AuthenticateModel model)
        {
            var user = _userService.Authenticate(model.Username, model.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha512Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info and authentication token
            return Ok(new
            {
                Id = user.Id,
                Username = user.Username,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Token = tokenString
            });
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]RegisterModel model)
        {
            model.Email = model.Username;

            // map model to entity
            var user = _mapper.Map<User>(model);

            try
            {
                // create user
                _userService.Create(user, model.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            var model = _mapper.Map<IList<UserModel>>(users);
            return Ok(model);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var user = _userService.GetById(id);
            var model = _mapper.Map<UserModel>(user);
            return Ok(model);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]UpdateModel model)
        {
            // map model to entity and set id
            var user = _mapper.Map<User>(model);
            user.Id = id;

            try
            {
                // update user 
                _userService.Update(user, model.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            return Ok();
        }

        [AllowAnonymous]
        [HttpPost("sendPasswordResetLink")]
        public IActionResult sendPasswordResetLink([FromBody]UserModel userModel)
        {
            var user = _userService.GetByEmail(userModel.Username);

            if (user == null)
                return Ok(); // Don't reveal that the user does not exist or is not confirmed

            // Send email to user with token informations
            // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
            // Send an email with this link

            string hashToken = _userService.GeneratePasswordResetToken(user.Id); // token will contains id + " " + expiration date + " " HASh(private secret + id + " " + expiration date) 

            //_userService.ResetPasswordToken(userIdentification, hashSecurity);
            // await UserManager.SendEmailAsync(user.Id, "Reset Password", "Please reset your password by clicking <a href=\"" + callbackUrl + "\">here</a>");          

            // Convert to text
            // StringBuilder Capacity is 128, because 512 bits / 8 bits in byte * 2 symbols for byte 
            //var hashedInputStringBuilder = new System.Text.StringBuilder(128);
            //foreach (var b in hashToken)
            //    hashedInputStringBuilder.Append(b.ToString("X2"));           

            return Ok(new { message = "Email sent" ,                          
                            hashToken = hashToken
            });

            //var callbackUrl = "Please reset your password by clicking <a href =\"" + callbackUrl + "\">here</a>"            
            // var callbackUrl = HttpContext Url.Action("ResetPassword", "Account", new { userId = user.Id, code = token }, protocol: Request.Url.Scheme);		
            // return RedirectToAction("ForgotPasswordConfirmation", "Account");
        }

        [AllowAnonymous]
        [HttpPost("isResetPasswordTokenMatch")]
        public IActionResult isResetPasswordTokenMatch([FromBody]GenTokenResetPasswordModel model)
        {
            int userId = _userService.ResetPasswordToken(model.HashToken); // get userId if the key match

            if (userId != 0)
            {               
                var user = _userService.GetById(userId);
                var rtnModel = _mapper.Map<UserModel>(user);

                return Ok(rtnModel); // return username
            }
            else
            {               
                return BadRequest(new { message = "Password reset date expired." } ); 
            }
        }

        [AllowAnonymous]
        [HttpPost("SaveResetPassword")]
        public IActionResult SaveResetPassword([FromBody]PasswordChangeModel model)
        {
            if (_userService.SaveResetPassword(model.UserName, model.Password))
                return Ok();
            else
                return BadRequest(new { message = "Error saving new password." });
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IEnumerable<User> GetAll();
        User GetById(int id);
        User GetByEmail(string email);
        User Create(User user, string password);
        void Update(User user, string password = null);
        void Delete(int id);
        string GeneratePasswordResetToken(int userId);
        int ResetPasswordToken(string hashSecurity);
        bool SaveResetPassword(string username, string password);
    }

    public class UserService : IUserService
    {
        private DataContext _context;

        public UserService(DataContext context)
        {
            _context = context;
        }

        public User Authenticate(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _context.Users.SingleOrDefault(x => x.Username == username);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            // authentication successful
            return user;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.Users;
        }

        public User GetById(int id)
        {
            return _context.Users.Find(id);
        }

        public User GetByEmail(string email)
        {
            return _context.Users.Where(x => x.Username.Equals(email)).SingleOrDefault();
        }
         
        public User Create(User user, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");            

            if (_context.Users.Any(x => x.Username == user.Username))
                throw new AppException("Email \"" + user.Username + "\" is already taken");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public void Update(User userParam, string password = null)
        {
            var user = _context.Users.Find(userParam.Id);

            if (user == null)
                throw new AppException("User not found");

            // update username if it has changed
            if (!string.IsNullOrWhiteSpace(userParam.Username) && userParam.Username != user.Username)
            {
                // throw error if the new username is already taken
                if (_context.Users.Any(x => x.Username == userParam.Username))
                    throw new AppException("Username " + userParam.Username + " is already taken");

                user.Username = userParam.Username;
            }

            // update user properties if provided
            if (!string.IsNullOrWhiteSpace(userParam.FirstName))
                user.FirstName = userParam.FirstName;

            if (!string.IsNullOrWhiteSpace(userParam.LastName))
                user.LastName = userParam.LastName;

            // update password if provided
            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            _context.Users.Update(user);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var user = _context.Users.Find(id);
            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
            }
        }

        // private helper methods

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }


        /// <summary>
        /// Password reset section
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="timeStamp"></param>
        public string GeneratePasswordResetToken(int userId)
        {                                     
            byte[] hashToken= null;

            using (var hmac = new System.Security.Cryptography.HMACSHA1())
            {
                hashToken = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes("unique token generated with hmac key"));
            }

            // disable all previous request            
            var passwordResetStillEnable = _context.PasswordReset.Where(c => c.UserId == userId && c.IsEnable == true).ToList();

            foreach (var passwordEnable in passwordResetStillEnable)
            {
                passwordEnable.IsEnable = false;
                _context.PasswordReset.Attach(passwordEnable);
                _context.Entry(passwordEnable).Property(x => x.IsEnable).IsModified = true;
                _context.SaveChanges();
            }

            System.Text.StringBuilder sbHashToken = new System.Text.StringBuilder(40);
            for (int i = 0, l = hashToken.Length; i < l; i++)
                sbHashToken.Append(hashToken[i].ToString("x2"));

            // create new one
            var resetPassword = new PasswordReset();
            resetPassword.UserId = userId;
            resetPassword.ExpirationDate = DateTime.Now.AddDays(3);
            resetPassword.HashToken = sbHashToken.ToString();     //Convert.ToBase64String(hashToken);
            resetPassword.IsEnable = true;
            
            _context.PasswordReset.Add(resetPassword);
            _context.SaveChanges();

            return sbHashToken.ToString();
        }

        public int ResetPasswordToken(string hashToken)
        {
            var passwordReset = _context.PasswordReset.Where(c => c.HashToken == hashToken && c.IsEnable == true).SingleOrDefault();

            if (passwordReset == null) return 0;
            if (DateTime.Now > passwordReset.ExpirationDate) return 0;
            
            return passwordReset.UserId;
        }

        public bool SaveResetPassword(string username, string password)
        {
            byte[] passwordHash;
            byte[] passwordSalt;

            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            var user = _context.Users.Where(c => c.Username == username).SingleOrDefault(); // verify if user is enable also

            if (user == null) return false;

            // update password
            //_context.Users.Attach(userPassword);
            //_context.Entry(userPassword).Property(x => x.PasswordHash).IsModified = true;
            //_context.Entry(userPassword).Property(x => x.PasswordSalt).IsModified = true;

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            _context.Users.Update(user);
            _context.SaveChanges();

            // update password reset flag
            var passwordEnable = _context.PasswordReset.Where(c => c.UserId == user.Id && c.IsEnable == true).SingleOrDefault();

            if (passwordEnable != null)
            {
                passwordEnable.IsEnable = false;
                passwordEnable.IsReset = true;

                _context.Update(passwordEnable);
                _context.SaveChanges();
            }
            

            return true;
        }

    }
}
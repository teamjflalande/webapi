using System;
using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Users
{
    public class GenTokenResetPasswordModel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string HashToken { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool IsReset { get; set; }
        public bool IsEnable { get; set; }       
    }
}